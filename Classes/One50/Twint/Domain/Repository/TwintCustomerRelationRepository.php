<?php
namespace One50\Twint\Domain\Repository;

/*
 * This file is part of the One50.Twint package.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class TwintCustomerRelationRepository extends Repository
{

    // add customized methods here

}
